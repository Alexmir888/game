﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spam : MonoBehaviour
{
    public Transform prefab;
    bool pos = false;
    int have = 0;
    public float time = 0.5f;
    public int max = 15;

    void FixedUpdate()
    {
        if (Input.GetMouseButton(0))
        {
            if (have <= max)
            {
                time = time - Time.deltaTime;
                if (time <= 0)
                {
                    if (pos == false)
                    {
                        Instantiate(prefab, transform.position + new Vector3(Random.Range(-0.5f, 0.5f), Random.Range(-0.5f, 0.5f), Random.Range(-0.5f, 0.5f)), Quaternion.identity);
                        pos = !pos;
                    }
                    else if (pos == true)
                    {
                        Instantiate(prefab, transform.position + new Vector3(Random.Range(-1, 1), Random.Range(-1, 1), Random.Range(-1, 1)), Quaternion.identity);
                        pos = !pos;
                    }
                    time = 0.5f;
                    have++;
                }
            }
        }
    }

}