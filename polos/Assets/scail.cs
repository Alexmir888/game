﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScalingAnimal : MonoBehaviour
{
    float scaling;
    // Start is called before the first frame update
    void Start()
    {
        scaling = Random.Range(0.4f, 2.0f);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonUp(0))
        {
            transform.localScale = new Vector3(scaling, scaling, scaling);
        }
    }
}